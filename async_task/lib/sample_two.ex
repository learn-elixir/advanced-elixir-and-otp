defmodule SampleTwo do
    use AsyncTask

    @timeout_response "Hi, Unknown!"
    task hello(name, _timer \\ 0) do
        "Hi, #{name}!"
    end
end