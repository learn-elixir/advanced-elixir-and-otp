defmodule Deck do
    use GenServer

    @name DeckServ

    def start_link() do
        GenServer.start_link(__MODULE__, [], name: @name)
    end

    def take_card() do
        GenServer.call @name, {:take_card}
    end

    def init(_) do
        {:ok, Enum.shuffle(1..52)}
    end

    def handle_call({:take_card}, _from, [card|deck]) do
        {:reply, card, deck}
    end
end
